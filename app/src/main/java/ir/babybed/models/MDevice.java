package ir.babybed.models;

/**
 * Created by SinaKH on 1/11/17.
 */

public class MDevice {
    private String name;
    private String address;

    public MDevice(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }
}
