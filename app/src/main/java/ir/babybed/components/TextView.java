package ir.babybed.components;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.util.AttributeSet;

import ir.babybed.utils.Constants;

public class TextView extends android.widget.TextView {

    public TextView(Context context, AttributeSet attrs, int defStyle){
        super(context,attrs,defStyle);
        init();
    }
    public TextView(Context context, AttributeSet attrs){
        super(context,attrs);
        init();
    }
    public TextView(Context context){
        super(context);
        init();
    }

    private void init() {
        if(!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), Constants.FONT);
            setTypeface(tf);
        }
    }

    private static final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();
    public void setTextStrike(String textStrike) {
        setText(textStrike, BufferType.SPANNABLE);
        Spannable spannable = (Spannable) getText();
        spannable.setSpan(STRIKE_THROUGH_SPAN, 0, textStrike.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }
}
