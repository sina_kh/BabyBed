package ir.babybed;

import android.Manifest;
import android.app.FragmentBreadCrumbs;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;

import ir.babybed.adapters.DevicesAdapter;
import ir.babybed.helpers.BluetoothService;
import ir.babybed.models.MDevice;

public class ChooseDeviceActivity extends AppCompatActivity {

    private BluetoothAdapter mBluetoothAdapter;

    private View bottomView;
    private View buttonsView;
    private TextView tvRotubat;
    private TextView tvDama;
    private RecyclerView recyclerView;
    private DevicesAdapter devicesAdapter;
    private ArrayList<MDevice> devices;
    private ImageView imgL;
    private ImageView imgR;

    private BluetoothService mChatService = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_device);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        bottomView = findViewById(R.id.viewBottom);
        buttonsView = findViewById(R.id.viewButtons);
        tvRotubat = (TextView) findViewById(R.id.tvRotubat);
        tvDama = (TextView) findViewById(R.id.tvDama);
        imgL = (ImageView) findViewById(R.id.img_l);
        imgR = (ImageView) findViewById(R.id.img_r);

        devices = new ArrayList<>();
        devicesAdapter = new DevicesAdapter(devices, this);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(devicesAdapter);

        //Turn on Bluetooth
        if (mBluetoothAdapter == null)
            Toast.makeText(this, "Your device doesnt support Bluetooth", Toast.LENGTH_LONG).show();
        else if (!mBluetoothAdapter.isEnabled()) {
            Intent BtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(BtIntent, 0);
            setTitle("در حال روشن کردن بلوتوث");
        }

        BroadcastReceiver mReceiver;
        mReceiver = new SingBroadcastReceiver();
        IntentFilter ifilterFound = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, ifilterFound);
        IntentFilter ifilterFinished = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(mReceiver, ifilterFinished);


        // Quick permission check
        int permissionCheck = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            permissionCheck = this.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
            permissionCheck += this.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
            if (permissionCheck != 0) {
                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number
                reListNow();
            } else {
                reListNow();
            }
        } else {
            reListNow();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        reListNow();
    }

    public void reListNow() {

        setTitle("در حال جستجو ی دستگاه ها ...");

        mBluetoothAdapter.startDiscovery();
    }

    public void itemSelected(int position) {
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(devices.get(position).getAddress());

        mChatService = new BluetoothService(this, mHandler);
        mChatService.connect(device, false);

        recyclerView.setVisibility(View.GONE);
        bottomView.setVisibility(View.VISIBLE);
        buttonsView.setVisibility(View.VISIBLE);
        setTitle("اتصال به دستگاه");
    }

    public void sendIt(View view) {
        Button btn = (Button) view;
        sendMessage(String.valueOf(btn.getText()) + "\r\n");
        imgL.setVisibility(View.GONE);
    }

    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (mChatService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, "اتصال برقرار نمیباشد", Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            mChatService.write(send);
        }
    }

    private class SingBroadcastReceiver extends BroadcastReceiver {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                devices.add(new MDevice(device.getName(), device.getAddress()));
                devicesAdapter.notifyDataSetChanged();
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                setTitle("انتخاب دستگاه");
            } else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                BluetoothAdapter mBluetoothAdapter;
                mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mBluetoothAdapter.isEnabled()) {
                    reListNow();
                } else {
                    setTitle("بلوتوث روشن شود.");
                }
            }

        }
    }

    private int activeReceivingMode = -1;
    private String humid = "";
    private String dama = "";
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BluetoothService.MESSAGE_READ:

                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);

                    for (int i = 0; i <= readMessage.length() - 1; i++) {
                        try {
                            String tmp = readMessage.substring(i, i + 1);

                            if (tmp.equalsIgnoreCase("R")) {
                                activeReceivingMode = 0;
                                humid = "";
                            } else if (tmp.equalsIgnoreCase("H")) {
                                activeReceivingMode = 1;
                                dama = "";
                            } else if (tmp.equalsIgnoreCase("L")) {
                                imgL.setVisibility(View.VISIBLE);
                            }

                            if (activeReceivingMode == 0) {
                                String regexStr = "^[0-9]*$";
                                if (tmp.matches(regexStr))
                                    humid = humid + tmp;
                                imgR.setVisibility(Integer.parseInt(humid) > 10 ? View.VISIBLE : View.GONE);
                            } else if (activeReceivingMode == 1) {
                                String regexStr = "^[0-9]*$";
                                if (tmp.matches(regexStr))
                                    dama = dama + tmp;
                            }

                            tvRotubat.setText("رطوبت:" + humid);
                            tvDama.setText("دما:" + dama);

                        } catch (NumberFormatException nfe) {
                        }
                    }

                    break;
                case BluetoothService.MESSAGE_TOAST:
                    // پیغامی در خصوص ارتباط و قطع آن رسید
                    message(msg.getData().getString(BluetoothService.TOAST), 0);
                    break;
            }
        }
    };

    public void message(CharSequence msg, int Dur) {
        // تابع نمایش پیغام
        Toast.makeText(this, msg, Dur).show();
    }

}
