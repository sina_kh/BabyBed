package ir.babybed.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ir.babybed.ChooseDeviceActivity;
import ir.babybed.R;
import ir.babybed.models.MDevice;

/**
 * Created by SinaKH on 1/11/17.
 */

public class DevicesAdapter extends RecyclerView.Adapter<DevicesAdapter.DeviceViewHolder> {

    private ArrayList<MDevice> devices;
    private ChooseDeviceActivity delegate;

    public DevicesAdapter(ArrayList<MDevice> devices, ChooseDeviceActivity chooseDeviceActivity) {
        this.devices = devices;
        this.delegate = chooseDeviceActivity;
    }

    public class DeviceViewHolder extends RecyclerView.ViewHolder {

        public TextView tvDeviceName;

        public DeviceViewHolder(View itemView, final ChooseDeviceActivity delegate) {
            super(itemView);
            // Handle item click and set the selection
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Redraw the old selection and the new
                    int focusedItem = getLayoutPosition();
                    delegate.itemSelected(focusedItem);
                }
            });
            tvDeviceName = (TextView) itemView.findViewById(R.id.item_device_name);
        }
    }

    @Override
    public DeviceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
               R.layout.item_device, parent, false);
        return new DeviceViewHolder(view, delegate);
    }

    @Override
    public void onBindViewHolder(DeviceViewHolder holder, int position) {
        bind(holder, position);
    }

    @Override
    public void onBindViewHolder(DeviceViewHolder holder, int position, List<Object> payload) {
        Log.d("butt", "payload " + payload.toString());
        bind(holder, position);
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    private void bind(final DeviceViewHolder holder, int position) {
        holder.tvDeviceName.setText(devices.get(position).getName() + " (" + devices.get(position).getAddress() + ")");
    }

}